extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

extern crate snake_game;

use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};

use snake_game::{Game, Snake, Grid};

const CELL_SIZE: f64 = 20.0;

fn main() {
    let opengl = OpenGL::V3_2;

    let (width, height) = (20, 20);

    let mut window: Window = WindowSettings::new("Hello there!", (width * CELL_SIZE as u32, height * CELL_SIZE as u32))
        .opengl(opengl)
        .srgb(false)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut game = Game {
        gl: GlGraphics::new(opengl),
        snake: Snake::new(),
        grid: Grid {width: (width as u8), height: (height as u8), cell_size: CELL_SIZE}
    };

    let mut events = Events::new(EventSettings::new()).ups(2);
    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            game.render(&r);
        }

        if let Some(u) = e.update_args() {
            game.update(&u);
        }

        if let Some(b) = e.button_args() {
            game.handle_input(&b);
        }
    }
}
