extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

use piston::input::*;
use opengl_graphics::GlGraphics;
use std::collections::LinkedList;

pub struct Game {
    pub gl: GlGraphics,
    pub snake: Snake,
    pub grid: Grid,
}

impl Game {
    pub fn render(&mut self, args: &RenderArgs) {
        self.grid.render(&mut self.gl, args);
        self.snake.render(&mut self.gl, args, self.grid.cell_size);
    }

    pub fn update(&mut self, _args: &UpdateArgs) {
        self.snake.update();
    }

    pub fn handle_input(&mut self, args: &ButtonArgs) {
        if let Button::Keyboard(k) = args.button {
            self.snake.handle_input(&k);
        }
    }
}

pub struct Grid {
    pub width: u8,
    pub height: u8,
    pub cell_size: f64
}

impl Grid {
    pub fn render(&self, gl: &mut GlGraphics, args: &RenderArgs) {
        use graphics::*;

        const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];

        gl.draw(args.viewport(), |_c, gl| {
            clear(GREEN, gl);
        });
    }
}


#[derive(Debug, Default)]
pub struct Snake {
    body: LinkedList<(u8, u8)>,
    direction: Direction
}

impl Snake {
    pub fn new() -> Snake {
        let mut body = LinkedList::new();
        body.push_front((0, 0));
        Snake { body, direction: Default::default() }
    }

    fn render(&mut self, gl: &mut GlGraphics, args: &RenderArgs, cell_size: f64) {
        use graphics;

        let red: [f32; 4] = [1.0, 0.0, 0.0, 1.0];

        let squares: Vec<graphics::types::Rectangle> = self.body.iter().map(|square| {
            graphics::rectangle::square(
            square.0 as f64 * cell_size,
            square.1 as f64 * cell_size,
            cell_size)
        }).collect();

        gl.draw(args.viewport(), |c, gl| {
            let transform = c.transform;
            squares.into_iter().for_each(|square| graphics::rectangle(red, square, transform, gl));
        })
    }

    fn update(&mut self) {
        let mut head = (*self.body.front().unwrap()).clone();
        
        match self.direction {
            Direction::Left => head.0 -= 1,
            Direction::Right => head.0 += 1,
            Direction::Up => head.1 -= 1,
            Direction::Down => head.1 += 1,
        }
        self.body.push_front(head);
        self.body.pop_back().unwrap();
    }

    fn handle_input(&mut self, key: &keyboard::Key) {
        let new_direction = match key {
            Key::Down => Direction::Down,
            Key::Up => Direction::Up,
            Key::Left => Direction::Left,
            Key::Right => Direction::Right,
            _ => self.direction
        };

        if new_direction != self.direction.reverse() {
            self.direction = new_direction;
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
enum Direction {
    Up, Down, Left, Right
}

impl Direction {
    fn reverse(&self) -> Direction {
        use Direction::*;
        match self {
            Up => Down,
            Down => Up,
            Left => Right,
            Right => Left
        }
    }
}

impl Default for Direction {
    fn default() -> Direction { Direction::Right }
}

enum IllegalMoveError {
    CollisionWithWall,
    CollisionWithItself
}
